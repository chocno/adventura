import logika.Hra;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;


/*******************************************************************************
 * Testovací třída BatohTest slouží ke komplexnímu otestování
 * třídy Batoh - popis viz komentář dole
 *
 * @author    Šimon Dlesk
 * @version   1.0
 */

public class BatohTest {

    private Hra hra;

    @Before
    public void setUp() {

        hra = new Hra();

    }

    // testujeme batoh. Nastavíme kapacitu batohu na 1 věc. Zaprvé, zda se věc přidá do batohu pokud je tam místo.
    // Druhá věc se tam ale již nevejde, protože již není místo.
    @Test
    public void seberTest() {
        hra.getHrac().getBatoh().setMaxVeci(1);
        Assert.assertEquals(hra.zpracujPrikaz("seber testak"), "věc přidána");
        Assert.assertEquals(hra.zpracujPrikaz("seber rustak"), "do batohu se nic nevejde");
    }
}

