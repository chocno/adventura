import logika.Hra;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;


/*******************************************************************************
 * Testovací třída SeberTest slouží k otestování toho , že nepřenostitelná věc opravdu nejde sebrat
 *
 * Testujeme příkaz seber (třída PrikazSeber)
 *
 * @author    Šimon Dlesk
 * @version   1.0
 */

public class SeberTest {

    private Hra hra;

    @Before
    public void setUp() {

        hra = new Hra();
    }

    @Test
    // testujeme, že nepřenostitelná věc opravdu nejde sebrat
    public void seberTest() {
        String a = hra.zpracujPrikaz("seber socha");
        Assert.assertEquals(a, "tuto věc nelze přenést");
    }
}
