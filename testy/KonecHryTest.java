import logika.Hra;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.*;

/*******************************************************************************
 * Testovací třída KonecHryTest slouží ke komplexnímu otestování, zda jde úspěšně ukončit hru (hráč sbalí 3 slečny / postavy). Viz komentář dole.
 *
 * @author    Šimon Dlesk
 * @version   1.0
 */

public class KonecHryTest {

    private Hra hra;
    private TextoveRozhrani textoveRozhrani;

    @Before
    public void setUp() {

        hra = new Hra();
    }

    @Test
    //Testujeme, že pokud hráč sbalí druhou slečnu, tak ještě není konec hry.
    public void konecTest() {
        hra.getHrac().setKolikMaSbaleno(1);
        hra.zpracujPrikaz("seber testak");
        hra.zpracujPrikaz("sbal blondyna");
        Assert.assertFalse(hra.konecHry());

    }

    @Test
    //Testujeme, že pokud hráč sbalí třetí slečnu, tak už je konec hry.
    public void konecTest2() {
        hra.getHrac().setKolikMaSbaleno(2);
        hra.zpracujPrikaz("seber testak");
        hra.zpracujPrikaz("sbal blondyna");
        Assert.assertTrue(hra.konecHry());
    }

    @Test
    // testujeme, zda hra má neúspěšný konec pro hráče, pokud mu dojdou životy. Tzn pokud použije příkaz jdi více než 24x. Tzn zda je mrtvý.
    public void neuspechTest() {
        String smer1 = "jdi karlin";
        String smer2 = "jdi zizkov";

        for(int i = 0; i < 24; i++) {
            if(i%2 == 0) {
                hra.zpracujPrikaz(smer1);
                System.out.println(hra.getHrac().getZivot());
            } else {
                hra.zpracujPrikaz(smer2);
                System.out.println(hra.getHrac().getZivot());
            }
        }
        Assert.assertTrue(hra.getHrac().isJeMrtvy());

    }
}