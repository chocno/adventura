package logika;

/**
 *  Třída Vec - trida ktera predstavuje Věci ve hře
 *  Každá věc je buď přenostielná nebo nepřenositelná.
 *  Každá věc má název, který ji identifikuje.
 *  Každá přenositelná věc přidá hráci sexappeal, ale bude ho stát určité množství peněz.
 *
 *  Tato třída je součástí jednoduché textové hry.
 *
 *@author     Šimon Dlesk
 *@version    1.0
 *@created    květen 2019
 */

public class Vec {
    private final String nazev;         // kazda vec ma nazev
    private final boolean prenositelna; // stejně tak informaci o tom, zda je přenostitelná
    private int sex;                    // každá věc přidá hráči jinou hodnotu sexappealu
    private int cena;                   // každá všc něco hráče stojí

    public Vec(String nazev, boolean prenositelna, int sex, int cena) {
        this.nazev = nazev;
        this.prenositelna = prenositelna;
        this.sex = sex;
        this.cena = cena;
    }

    public String getNazev() {
        return nazev;
    }

    public boolean isPrenositelna() {
        return prenositelna;
    }

    public int getSex() {
        return sex;
    }

    public int getCena() {
        return cena;
    }
}
