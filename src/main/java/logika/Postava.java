package logika;

/**
 *  Třída Postava - trida ktera predstavuje postavy ve hře
 *  Každá postava vyžaduje určitý sexappeal na to, aby byla úspěšně sbalena.
 *  Každá postava má jméno, které ji identifikuje.
 *
 *  Tato třída je součástí jednoduché textové hry.
 *
 *@author     Šimon Dlesk
 *@version    1.0
 *@created    květen 2019
 */

public class Postava {

    private String jmeno;   // každá postava má jméno
    private int sex;        // každá postava má uvedenou potřebnou hodnotu sexappealu na její sbalení

    public Postava(String jmeno, int sex) {
        this.jmeno = jmeno;
        this.sex = sex;
    }

    public String getJmeno() {
        return jmeno;
    }

    public int getSex() {
        return sex;
    }
}
