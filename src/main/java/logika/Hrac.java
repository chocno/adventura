package logika;

/**
 *  Třída Hrac - trida ktera predstavuje postavu / Hráče, za kterého hrajete ve hře
 *  Hráč má 24 životů. Začíná se 100 Kč a se sexappealem na 0, který poté zvyšuje sbíráním / kupováním věcí.
 *  Hráč poté balí holky / postavy ve hře, čímž se mu zvyšuje skore. K výhře potřebuje sbalit 3 postavy.
 *  Pokud hráči dojdou životy, zemře.
 *
 *  Tato třída je součástí jednoduché textové hry.
 *
 *@author     Šimon Dlesk
 *@version    1.0
 *@created    květen 2019
 */

public class Hrac {
    private Batoh batoh;                // hráč má batoh
    private int zivot = 24;             // hráč má omezený počet životů (životy se ubírají při použití příkazu jdi a sbal)
    private int penize = 100;           // hráč má omezený počet peněz, za které může nakupovat věci (každá věc něco stojí)
    private int sex = 0;                // hráč začína s nulovým sexappealem, který mu zvyšují nakoupené věci - pomocí toho dokáže sbalit slečny
    private int kolikMaSbaleno = 0;     // informace o tom, kolik holek už hráč úspěšně sbalil (k výtězství potřebuje 3)
    boolean jeMrtvy = false;            // údaj o tom, zda je hráč živý nebo mrtvý (pokud mu dojdou životy)

    public Hrac() {

        batoh = new Batoh();
    }

    // informace o tom, kolik má hráč peněz
    public int getPenize() {
        return penize;
    }

    // informace o tom, kolim má hráč životů
    public int getZivot() {
        return zivot;
    }

    public Batoh getBatoh() {
        return batoh;
    }

    public void setPenize(int penize) {
        this.penize = penize;
    }

    // informace o tom, kolik má hráč sexappealu
    public int getSex() {
        return sex;
    }

    // informace o tom, zda je hráč mrtvý
    public boolean isJeMrtvy() {
        return jeMrtvy;
    }

    public void setJeMrtvy(boolean jeMrtvy) {
        this.jeMrtvy = jeMrtvy;
    }

    // informace o tom, kolik holek má hráč sbaleno
    public int getKolikMaSbaleno() {
        return kolikMaSbaleno;
    }

    // pro účel testu, nastavení toho, kolik má hráč sbaleno
    public void setKolikMaSbaleno(int kolikMaSbaleno) {
        this.kolikMaSbaleno = kolikMaSbaleno;
    }

    public void setSex(int sex) {
        this.sex = sex;
    }

    public void setZivot (int zivot) {
        this.zivot = zivot;
    }

    // tato metoda se používá v rámci příkazu PrikazVypisInfo, slouží k tomu že vrátí aktuální stav peněž, životů a kolik holek je sbaleno.
    public String vypisVlastnosti () {
        return "peníze: " + getPenize() + " život: " + getZivot() +" kolik si sbalil: " + getKolikMaSbaleno();
    }


}

