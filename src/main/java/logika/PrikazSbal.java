package logika;


/**
 *  Třída PrikazSbal – Hráč se pokusí sbalit postavu, která je v jeho prostoru. Pokud uspěje, tak je o krok blíže k vítezství a odečtou se mu 4 životy.
 *  Pokud je naopak ve svém pokusu neúspěšný, tak se mu odečtou 3 životy.
 *  Na to aby hráč postavu sbalil, tak musí mít dostatečný sexappeal, který mu přidávají věci, které si nakoupí.
 *
 *  Tato třída je součástí jednoduché textové hry.
 *
 *@author     Šimon Dlesk
 *@version    1.0
 *@created    květen 2019
 */

public class PrikazSbal implements IPrikaz {
    HerniPlan plan;
    Hrac hrac;
    private Hra hra;
    public PrikazSbal(HerniPlan plan, Hrac hrac, Hra hra) {
        this.plan = plan;
        this.hrac = hrac;
        this.hra = hra;
    }
    public static final String NAZEV = "sbal";
    @Override
    // Nejprve zkontolujeme, zda hráč zadal, koho má sbalit. Poté zjistíme, zda postava existuje a dále zkontrolujeme zda má dostatečný sexappeal na to postavu sbalit.
    // Nakonec přidáme hráčovi +1 bod do jeho score - kolik má sbaleno holek.
    // Za každý neúspššný pokus o sbalení se hráči odečte 6 životů.
    public String provedPrikaz(String... parametry) {
        if(parametry.length == 0) {
            return "nevim koho  mam sbalit";
        } else {
            String jmeno = parametry[0];
            if(plan.getAktualniProstor().postavaExistuje(jmeno)) {
                if(plan.getAktualniProstor().getPostava(jmeno).getSex() > hrac.getSex()) {
                    hrac.setZivot(hrac.getZivot() - 3);
                    if(hrac.getZivot() <= 0) {
                        hrac.setJeMrtvy(true);
                        System.out.println("Zemřel si ! GAME OVER !");
                        }
                    return "Nepodarila se ti sbalit" + "\nSbalíš mě, až se sexappealem: " + plan.getAktualniProstor().getPostava(jmeno).getSex()  ;
                }else {
                    hrac.setKolikMaSbaleno(hrac.getKolikMaSbaleno() + 1);
                    if(hrac.getKolikMaSbaleno() == 3) {
                       hra.setKonecHry(true);
                       System.out.println("VYHRÁL SI! SBALIL SI 3 SLEČNY!");
                    }
                    plan.getAktualniProstor().odstranPostavu(jmeno);
                    hrac.setZivot(hrac.getZivot() - 4);
                    if(hrac.getZivot() <= 0) {
                        hrac.setJeMrtvy(true);
                        System.out.println("Zemřel si ! GAME OVER !");
                    }
                    return "SBALIL SI ";
                }
            } else {
                return "postava tady neni";
            }
        }
    }

    @Override
    public String getNazev() {
        return NAZEV;
    }
}
