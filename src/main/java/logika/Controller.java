package logika;

import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Node;
import javafx.scene.control.ButtonType;
import javafx.scene.control.Dialog;
import javafx.scene.control.Label;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.HBox;
import javafx.scene.layout.StackPane;
import javafx.scene.layout.VBox;
import javafx.scene.web.WebEngine;
import javafx.scene.web.WebView;

import java.io.IOException;
import java.io.InputStream;
import java.util.Optional;

public class Controller {

    public static final int SIRKA_IKONY = 45;
    public static final int VYSKA_IKONY = 30;

    @FXML
    private VBox seznamVychodu;
    @FXML
    private VBox seznamPredmetuVMistnosti;
    @FXML
    private VBox seznamPredmetuVBatohu;
    private Hra hra;

    public ImageView obrazekLokace;
    @FXML
    private Label popisLokace;
    @FXML
    private Label jmenoLokace;
    @FXML
    private VBox seznamPostav;

    @FXML
    private Label akcniDialog;

    @FXML
    private StackPane hlavniLista;


    public void setHra(Hra hra) {
        this.hra = hra;
        HerniPlan herniPlan = hra.getHerniPlan();
        Prostor aktualniProstor = herniPlan.getAktualniProstor();
        zmenProstor(aktualniProstor);
        akcniDialog.setText(hra.vratUvitani());
    }

    private void zmenProstor(Prostor prostor) {
        if (hra.getHrac().getZivot() > 0) {
            System.out.println(hra.getKonec());
            hra.zpracujPrikaz("jdi " + prostor.getNazev());
            System.out.println(hra.getHerniPlan().getAktualniProstor().getNazev());

            jmenoLokace.setText(prostor.getNazev());
            popisLokace.setText(prostor.getPopis());

            String nazevObrazku = "/" + prostor.getNazev() + ".jpg";
            Image image = new Image(getClass().getResourceAsStream(nazevObrazku));
            obrazekLokace.setImage(image);

            pridejVychody(prostor);
            pridejPredmety(prostor);
            pridejPostavy(prostor);
        }
        else {
            akcniDialog.setText("Prohral si ");
        }
    }

    private void pridejVychody(Prostor prostor) {
        seznamVychodu.getChildren().clear();
        for (Prostor p : prostor.getVychody()) {
            HBox vychod = new HBox();
            vychod.setSpacing(10);
            Label nazevProstoru = new Label(p.getNazev());

            ImageView vychodImageView = new ImageView();
            Image vychodImage = new Image(getClass().getClassLoader().getResourceAsStream("\\" + p.getNazev() + ".jpg"));
            vychodImageView.setFitHeight(VYSKA_IKONY);
           vychodImageView.setFitWidth(SIRKA_IKONY);
            vychodImageView.setImage(vychodImage);


            vychod.getChildren().addAll(vychodImageView, nazevProstoru);

            seznamVychodu.getChildren().add(vychod);
            vychod.setOnMouseClicked(event -> {
                zmenProstor(p);
            });
        }
    }


    //----------------------------------------------------------------------------------------------------------------------------------

    private void pridejPostavu(Postava postava) {
        HBox postavaBox  = new HBox();
        postavaBox.setSpacing(10);
        postavaBox.getStyleClass().add("kontejner");
        ImageView postavaImageView = new ImageView();
        Image predmetImage = new Image(getClass().getClassLoader().getResourceAsStream("\\" + postava.getJmeno() + ".jpg"));
        postavaImageView.setFitHeight(VYSKA_IKONY);
        postavaImageView.setFitWidth(SIRKA_IKONY);
        postavaImageView.setImage(predmetImage);
        Label nazevPostavy = new Label(postava.getJmeno());
        postavaBox.getChildren().addAll(postavaImageView, nazevPostavy);
        seznamPostav.getChildren().add(postavaBox);
        postavaBox.setOnMouseClicked(event -> {
            if(!hra.getHrac().jeMrtvy) {
                akcniDialog.setText(hra.zpracujPrikaz("sbal " + postava.getJmeno()));
                if (hra.getHrac().getKolikMaSbaleno() == 3) {
                    akcniDialog.setText("vyhral si, seš Baller");
                    hra.getHrac().setZivot(2000000000);
                }
            } else {
                akcniDialog.setText("konec hry");
            }
        });

    }


    //--------------------------------------------------------------------------------------------------------------------------------------


    private void pridejPostavy(Prostor prostor) {
        seznamPostav.getChildren().clear();
        for(Postava postava : prostor.getPostavy()) {
            pridejPostavu(postava);
        }
    }

    private void pridejPredmety(Prostor prostor) {
        seznamPredmetuVMistnosti.getChildren().clear();

        for (Vec vec : prostor.getSeznamVeci()) {
            pridejPredmetDoMistnosti(vec);
        }
    }

    private void pridejPredmetDoMistnosti(Vec vec) {
        HBox predmet = new HBox();
        predmet.setSpacing(10);
        Label nazevVeci = new Label(vec.getNazev());
        ImageView predmetImageView = new ImageView();
        Image predmetImage = new Image(getClass().getClassLoader().getResourceAsStream("\\" + vec.getNazev() + ".jpg"));
        predmetImageView.setFitHeight(VYSKA_IKONY);
        predmetImageView.setFitWidth(SIRKA_IKONY);
        predmetImageView.setImage(predmetImage);

        predmet.getChildren().addAll(predmetImageView,nazevVeci);
        seznamPredmetuVMistnosti.getChildren().add(predmet);
        predmet.setOnMouseClicked(event -> {
            if (vec.isPrenositelna()) {
                HBox vecBatoh = new HBox();
                vecBatoh.setSpacing(10);
                String prikaz = hra.zpracujPrikaz("seber " + vec.getNazev());
                akcniDialog.setText(prikaz);
                if (!prikaz.equals("nemas na to cash")) {
                    Label vecVBatohu = new Label(vec.getNazev());
                    ImageView predmetBatohImageView = new ImageView();
                    Image predmetBatohImage = new Image(getClass().getClassLoader().getResourceAsStream("\\" + vec.getNazev() + ".jpg"));
                    vecBatoh.getChildren().addAll(predmetImageView, vecVBatohu);
                    seznamPredmetuVBatohu.getChildren().add(vecBatoh);
                    seznamPredmetuVMistnosti.getChildren().remove(predmet);
                }

                vecBatoh.setOnMouseClicked(event1 -> {
                    hra.zpracujPrikaz("odeber "+vec.getNazev());
                    seznamPredmetuVBatohu.getChildren().remove(vecBatoh);
                    pridejPredmetDoMistnosti(vec);
                });
            }
            else {
                akcniDialog.setText("Věc není přenositelná, střelče!");
            }
        });
    }
    @FXML

    public void novaHra() {
        setHra(new Hra());
        seznamPredmetuVBatohu.getChildren().clear();


    }

    @FXML
    public void ukazNapovedu() {
        Dialog<ButtonType> dialog = new Dialog<>();
        dialog.initOwner(hlavniLista.getScene().getWindow());
        dialog.setTitle("Napoveda");
        try {
            WebView view = new WebView();
            WebEngine engine = view.getEngine();
            engine.load(String.valueOf(getClass().getResource("/napoveda.html")));
            dialog.getDialogPane().setContent(view);
        } catch (Exception e) {
            System.out.println(e);
        }
        dialog.getDialogPane().getButtonTypes().add(ButtonType.CANCEL);
        dialog.showAndWait();

    }

    @FXML
    public void ukazMapu() {
        Dialog<ButtonType> dialog = new Dialog<>();
        dialog.initOwner(hlavniLista.getScene().getWindow());
        dialog.setTitle("Mapa");
//        FXMLLoader fxmlLoader = new FXMLLoader();
//        fxmlLoader.setLocation(getClass().getResource("/mapa.fxml"));
        String nazevObrazku = "/" +  hra.getHerniPlan().getAktualniProstor().getNazev() + ".jpg";
        System.out.println(nazevObrazku);
        Image mapaImage = new Image(getClass().getResourceAsStream(nazevObrazku));
        ImageView view = new ImageView();
        view.setImage(mapaImage);
        view.setFitWidth(500);
        view.setFitHeight(500);
        Label label = new Label();
        dialog.getDialogPane().setContent(view);
        dialog.getDialogPane().getButtonTypes().add(ButtonType.CANCEL);
        dialog.showAndWait();

    }

    @FXML
    public void konecHry() {
        System.exit(0);
    }
}

