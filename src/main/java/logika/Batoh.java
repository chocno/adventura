package logika;

import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

/**
 *  Třída Batoh - trida ktera predstavuje inventar postavy (věci jsou v HashMapě)
 *  Maximální počet věcí v batohu jsou 3
 *
 *  Tato třída je součástí jednoduché textové hry.
 *
 *@author     Šimon Dlesk
 *@version    1.0
 *@created    květen 2019
 */

public class Batoh {
    // maximalni pocet veci ktere se vejdou do batohu
    public int maxVeci = 3;
    //sbirat vice veci se stejnym nazvem ve hre nejde
    private HashMap<String, Vec> seznamVeci;
    //pro testovaci ucely je mozne zadat velikost batohu
    public Batoh(int pocet) {
        seznamVeci = new HashMap<>();
        maxVeci = pocet;
    }
    public Batoh() {
        seznamVeci = new HashMap<>();
    }

    /**
     *
     * @param vec
     * @return boolean hodnota pro testovani
     * umoznuje vkladat vec do batohu, pokud neni plny
     */
    public boolean vlozDoBatohu(Vec vec) {
        if(seznamVeci.size() == maxVeci) {
            System.out.println("Do batohu se ti už nic nevejde");
            return false;
        }
        else {
            seznamVeci.put(vec.getNazev(), vec);
            return true;
        }
    }

    //získám věc z batohu podle jejího názvu a metoda vyhledá věc v hash mapě
    public Vec ziskejVecZBatohu(String nazev) {
        return seznamVeci.get(nazev);
    }


    /**
     *
     * @param nazev
     * @return boolean hodnota pro testy
     * umoznuje odstranit vec z batohu, pokud se v batohu nachazi
     */

    //Odstraní věc z batohu. Na vstupu máme název. Pokud název odpovídá klíči k nějaké věci, tak ji z batohu odstraníme. Pokud věc v a batohu není, tak se vypíše "Tuhle vec v batohu nemam"
    public boolean odstranZBatohu(String nazev) {
        if(seznamVeci.containsKey(nazev)) {
            seznamVeci.remove(nazev);
            return true;
        }
        else {
            System.out.println("Tuhle vec v batohu nemam");
            return false;
        }
    }

    /**
     *
     * @return String reprezentujici seznam veci v batohu
     */

    //tuto metodu ve hře používám k tomu, když vypisuju batoh pomocí příkazu PrikazVypisInfo - "vypis batoh" - ve hře zjistím, co mám v batohu
    public String vypisSeznamVeci() {
        if(seznamVeci.size() == 0) {
            return "batoh je prázdný";
        }
        StringBuilder b = new StringBuilder();
        for (Object o : seznamVeci.entrySet()) {
            Map.Entry prvek = (Map.Entry) o;
            b.append(prvek.getKey()).append(" ");
        }
        return b.toString().substring(0, b.length() - 1);
    }

    //pomocí této metody můžeme podle názvu věci zjistit, zda se věc nachází v batohu
    public boolean jeVBatohu(String vec) {
        return seznamVeci.containsKey(vec);
    }

    //kvůli testu je zde možnost nastavit maximální počet věcí, které se vejdou do batohu
    public void setMaxVeci(int maxVeci) {
        this.maxVeci = maxVeci;
    }
}
