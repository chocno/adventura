package logika;


/**
 *  Class HerniPlan - třída představující mapu a stav adventury.
 * 
 *  Tato třída inicializuje prvky ze kterých se hra skládá:
 *  vytváří všechny prostory,
 *  propojuje je vzájemně pomocí východů 
 *  a pamatuje si aktuální prostor, ve kterém se hráč právě nachází.
 *
 *@author     Michael Kolling, Lubos Pavlicek, Jarmila Pavlickova
 *@version    pro školní rok 2016/2017
 */
public class HerniPlan {
    
    private Prostor aktualniProstor;
    
     /**
     *  Konstruktor který vytváří jednotlivé prostory a propojuje je pomocí východů.
     *  Jako výchozí aktuální prostor nastaví halu.
     */
    public HerniPlan() {
        zalozProstoryHry();

    }
    /**
     *  Vytváří jednotlivé prostory a propojuje je pomocí východů.
     *  Jako výchozí aktuální prostor nastaví domeček.
     */
    private void zalozProstoryHry() {
        // vytvářejí se jednotlivé prostory
        Prostor zizkov = new Prostor("zizkov","Žižkov");
        Prostor vinohrady = new Prostor("vinohrady", "Vinohrady");
        Prostor karlin = new Prostor("karlin","Karlín");
        Prostor vrsovice = new Prostor("vrsovice","Vršovice");
        Prostor holesovice = new Prostor("holesovice","Holešovice");
        Prostor liben = new Prostor("liben","Libeň");
        Prostor smichov = new Prostor("smichov","Smíchov");
        Prostor nove_mesto = new Prostor("nove_mesto","Nové město");

        // Vytvářím nové postavy
        zizkov.pridejPostavu(new Postava("blondyna",1));
        karlin.pridejPostavu(new Postava("jolanda",10));
        holesovice.pridejPostavu(new Postava("babicka",8));
        liben.pridejPostavu(new Postava("gay",7));
        nove_mesto.pridejPostavu(new Postava("patnactka",10));
        smichov.pridejPostavu(new Postava("prostitutka",10000000));
        vrsovice.pridejPostavu(new Postava("fitness_girl",16));
        vinohrady.pridejPostavu(new Postava("bruneta",6));
        // Vytvářím nové věci

        zizkov.pridejVec(new Vec ("blondynometr",true, 1, 59));
        zizkov.pridejVec(new Vec ("socha",false, 0, 0));
        karlin.pridejVec(new Vec ("permice_fitko",true, 5, 39));
        holesovice.pridejVec(new Vec ("sako",true, 3, 19));
        liben.pridejVec(new Vec ("spendlik",true, 1, 39));
        nove_mesto.pridejVec(new Vec ("jidlo",true, 3, 29));
        smichov.pridejVec(new Vec ("trenbolon",true, 100, 99));
        smichov.pridejVec(new Vec ("dum",false, 0, 0));
        vrsovice.pridejVec(new Vec ("testak",true, 9, 39));
        vrsovice.pridejVec(new Vec ("retizek",true, 2, 0));
        vinohrady.pridejVec(new Vec ("makeup",true, 4, 49));

        // přiřazují se průchody mezi prostory (sousedící prostory)
        zizkov.setVychod(vinohrady);
        zizkov.setVychod(karlin);
        zizkov.setVychod(liben);
        vinohrady.setVychod(zizkov);
        vinohrady.setVychod(nove_mesto);
        vinohrady.setVychod(vrsovice);
        karlin.setVychod(holesovice);
        karlin.setVychod(liben);
        karlin.setVychod(zizkov);
        karlin.setVychod(nove_mesto);
        liben.setVychod(zizkov);
        liben.setVychod(karlin);
        liben.setVychod(holesovice);
        holesovice.setVychod(liben);
        holesovice.setVychod(karlin);
        holesovice.setVychod(nove_mesto);
        nove_mesto.setVychod(smichov);
        nove_mesto.setVychod(karlin);
        nove_mesto.setVychod(holesovice);
        smichov.setVychod(nove_mesto);
        vrsovice.setVychod(vinohrady);


                
        aktualniProstor = zizkov;  // hra začíná na zizkove
    }
    
    /**
     *  Metoda vrací odkaz na aktuální prostor, ve ktetém se hráč právě nachází.
     *
     *@return     aktuální prostor
     */
    
    public Prostor getAktualniProstor() {
        return aktualniProstor;
    }
    
    /**
     *  Metoda nastaví aktuální prostor, používá se nejčastěji při přechodu mezi prostory
     *
     *@param  prostor nový aktuální prostor
     */
    public void setAktualniProstor(Prostor prostor) {
       aktualniProstor = prostor;
    }

}
