package logika;
/**
 *  Třída PrikazOdeber - Odebere věc z batohu neboli ji prodá za 22 Kč. Na některých věcech se dá založit business a vydělávat tak peníze. Nakoupím levněji a prodám dráž.
 *
 *  Tato třída je součástí jednoduché textové hry.
 *
 *@author     Šimon Dlesk
 *@version    1.0
 *@created    květen 2019
 */

public class PrikazOdeber implements IPrikaz {
    public static final String NAZEV = "odeber";
    HerniPlan plan;
    Hrac hrac;
    public PrikazOdeber(HerniPlan plan, Hrac hrac) {
        this.plan = plan;
        this.hrac = hrac;
    }

    @Override
    // Nejprve zkontrolujeme, zda hráč zadal jakou věc chce odebrat. Poté věc získáme z batohu a dáme ji zpět do aktuálního prostoru.
    // Dostaneme nazpšt prodejní cenu, která bude ale pouze 22 Kč. Na nějakých věcech je tímto možné i vydělávat. Koupím levněji a prodám dráž.
    public String provedPrikaz(String... parametry) {
                if(parametry.length == 0) {
            return "Nevím, jakou věc mám odebrat";
        }
        else{
            String nazev = parametry[0];
            if(hrac.getBatoh().jeVBatohu(nazev)) {
                Vec vec = hrac.getBatoh().ziskejVecZBatohu(nazev);
                hrac.getBatoh().odstranZBatohu(nazev);
                plan.getAktualniProstor().pridejVec(vec);
                hrac.setPenize(22 + hrac.getPenize());
                return "tato věc byla odstraněna: " + nazev;
            }
            else {
                return "Věc se nenachází v batohu";
            }
        }
    }

    @Override
    public String getNazev()
    {
        return NAZEV;
    }
}
