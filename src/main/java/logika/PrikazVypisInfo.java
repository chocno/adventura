package logika;

/**
 *  Třída PrikazVypisInfo – Zde si může hráč vypsat aktuální stav batohu a nebo počet peněz, životů a kolik holek má sbaleno (hrač napíše: vypis batoh nebo vypis vlastnosti).
 *
 *  Tato třída je součástí jednoduché textové hry.
 *
 *@author     Šimon Dlesk
 *@version    1.0
 *@created    květen 2019
 */

public class PrikazVypisInfo implements IPrikaz {
    private static final String NAZEV = "vypis";
    Hrac hrac;

    // Nejprve zkontolujeme, zda hráč zadal, jaké informace chce vypsat. Může vypsat but stav batohu a nebo jeho vlastnosti.
    // Batoh - vypíše věci v batohu (třída batoh metoda vypisSeznamVeci)
    // Vlastnosti - Vypíše aktuální stav peněž, životů a kolik holek je sbaleno (třída Hráč metoda vypisVlastnosti)
    public PrikazVypisInfo(Hrac hrac) {
        this.hrac = hrac;
    }
    @Override
    public String provedPrikaz(String... parametry) {
        if(parametry.length == 0) {
            return "nevím jaké informace myslíš";
        }
        else {
            String informace = parametry[0];
            if(informace.equals("batoh")) {
                return hrac.getBatoh().vypisSeznamVeci();
            } else if(informace.equals("vlastnosti")) {
                return hrac.vypisVlastnosti();
            } else {
                return "nevim co myslis";
            }
        }

    }

    @Override
    public String getNazev() {
        return NAZEV;
    }
}
