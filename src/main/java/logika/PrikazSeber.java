package logika;

/**
 *  Třída PrikazSeber – Tímto příkazem hráč sebere / nakoupí věc, která mu následně zvýší sexappeal. Odečtou se mu peníze.
 *
 *  Tato třída je součástí jednoduché textové hry.
 *
 *@author     Šimon Dlesk
 *@version    1.0
 *@created    květen 2019
 */

public class PrikazSeber implements IPrikaz {

    private final static String NAZEV = "seber";
    private final HerniPlan herniPlan;
    private Hrac hrac;

    public PrikazSeber(HerniPlan plan, Hrac hrac) {
        herniPlan = plan;
        this.hrac = hrac;

    }
    @Override
    // Nejprve zkontolujeme, zda hráč zadal, co má sebrat / koupit. Poté zda věc existuje. Poté, zda na ni má penize.
    // Poté pokud se věc do batohu vejde, tak se do něj přidá a zvýší hráči sexappeal.
    public String provedPrikaz(String... parametry) {
        if(parametry.length == 0) {
            return "nevím co mám sebrat";
        }

        String nazevVeci = parametry[0];
        Prostor prostor = herniPlan.getAktualniProstor();
        if(!prostor.vecExistuje(nazevVeci)) {
            return "věc neexistuje";
        }
        Vec vec = prostor.ziskejVec(nazevVeci);
        if(vec.isPrenositelna()) {
            if(vec.getCena() > hrac.getPenize() ) {
                return "nemas na to cash";
            }
            else {
                if(!hrac.getBatoh().vlozDoBatohu(vec)) {
                    return "do batohu se nic nevejde";
                }
                prostor.odstranVec(nazevVeci);
                hrac.setPenize(hrac.getPenize() - vec.getCena());
                hrac.setSex(hrac.getSex() + vec.getSex());
                return "věc přidána";
            }
        }
        return "tuto věc nelze přenést";
    }

    @Override
    public String getNazev() {
        return NAZEV;
    }



}
